/**************************************************************************/
/*!
  @section Overview

  This software serves as a control algorithm for a geyser boiler temperature data acquisition system. 
  The temperature sensor that will be used mainly is the DS18B20. 
  
  The DS18B20 is best suited based on the following: 

    1. Rated accuracy is +- 0.5 C (It is different for every temperature region)
    2. Can read temperatures between -10 C and +85 C
    3. All sensors can be connected on a single bus thanks to the OneWire protocol implemented
    4. The DS18B20 has a built-in ADC which provides temperature readings with a resolution up to 12-bit (9-bit lowest)
    5. Each DS18B20 sensor has a 8-byte ROM address that can be used to reference a specific sensor on the bus. It also 
        allows for many, many devices to be connected on a single bus.
    6. The sensor version used is a waterproof (To some extent) version with a stainless steel probe cover - making it ideal
        for a submerged water usage. 

  This project forms part of a master's thesis at Stellenbosch University's Electrical and Electronic Engineering department. 
  
  This source code will be used to control a microcontroller (Arduino Due) device to capture temperature data points at 
  specific regions of a typical geyser boiler. This is to identify the thermal profile based on water usage data such as 
  volume used and water usasge schedules. It will also be used to verify the work of the "Two-node" geyser model and possibly
  improve it.

  @author Daniel van Schalkwyk
          Student number: 19891334

*/
/**************************************************************************/

// Include all header files
#include "Boiler_DAQ_Control.h"

void setup() {  // The setup code for the microcontroller
  Serial.begin(SerialBaudrate);    // Establishes a serial connection with a baudrate of 200000
  oneWireSetup();             // Set up the oneWire Temperature sensors
  sdCardSuccessFlag = sdCardSetup();  // Set up the SD card
  
}

void loop() {   // The main loop of the microcontroller. 
  void discoverOneWireDevices(void);
}

void oneWireSetup() {

  for (int i = 0; i < boilerProfileBusNumber; i++)
    {
        TempBusses[i] = DallasTemperature(&OneWireBusses[i]);
          // TempBusses[0] : Full vertical sensor assembly furthest from element
          // TempBusses[1] : Full vertical sensor assembly next to TempBus0 closer to element
          // TempBusses[2] : Full vertical sensor assembly next to TempBus1 closer to element
          // TempBusses[3] : Full vertical sensor assembly closest to boiler element
          // TempBusses[4] : Two short/vertical sensor assemblies above and below element
          // TempBusses[5] : Top-side (left) sensor assembly 
          // TempBusses[6] : Top-side (right) sensor assembly 
          // TempBusses[7] : This bus includes both Bottom-side sensor assemblies of the system

        TempBusses[i].begin();  //  Initialise all DS18B20 sensors on all busses
        tempDeviceCount[i] = TempBusses[i].getDeviceCount();  //  Get the sensor count of each bus
        TempBusses[i].setResolution(tempResolution);  // Set the temperature resolution of all busses
        TempBusses[i].setWaitForConversion(false);  // Ensure that the MCU knows not to wait for any conversion to alleviate blocking
    }
}

bool sdCardSetup()  {
  bool sdCardSuccess;
  if(captureBoilerData)
  {
    // See if the card is present and can be initialized:
    if (!SD.begin(sd_CS)) {
      Serial.println("SD Card failed or is not present");
      sdCardSuccess = false;
    }
    sdCardSuccess = true;
    Serial.println("SD card Successfully Initialized");
  }
  return sdCardSuccess;
}

// void sendDataToSD(String dataString) {
//   // This function takes the sampled data and sends it to the appropriate file on the SD card
//   DateTime now = sysRTC.now();
//   String fileName = "";
//   if(captureBoilerData)
//   {
//     if(newFileFlag)
//     {
//       fileName += String(now.day());
//       fileName += String(now.hour());
//       fileName += String(now.minute());
//       fileName += String(now.second());
//       fileName += ".csv";
//       Serial.println("New file created: " + fileName);
//       File dataFile = SD.open(fileName, FILE_WRITE);
//       delay(10);
//       if (dataFile) 
//       {
//         dataFile.println(dataColNames);
//         dataFile.println(dataColUnits);
//         dataFile.close();
//         Serial.println(dataColNames);
//       }
//       else
//       {
//         Serial.print("File creation failed");
//       }
//       newFileFlag = LOW;      
//     }

//     File dataFile = SD.open(fileName, FILE_WRITE);
//     // if the file is available, write to it:
//     if (dataFile) {
//       if(dataReadyFlag) 
//       {
//         dataFile.println(dataString);
//         dataReadyFlag = false; 
//         Serial.println(dataString);
//       }
//       dataFile.close();
//     }
//     // if the file isn't open, pop up an error:
//     else {
//       Serial.println("Error opening file");
//     }
//   }
// }

void discoverOneWireDevices(void) {
  // Open-source code used to easily find the OneWire Addresses of sensors 
  byte i;
  byte addr[8];
  
  Serial.print("Looking for 1-Wire devices...\n\r");
  while(ds.search(addr)) {
    Serial.print("\n\rFound \'1-Wire\' device with address:\n\r");
    for( i = 0; i < 8; i++) {
      Serial.print("0x");
      if (addr[i] < 16) {
        Serial.print('0');
      }
      Serial.print(addr[i], HEX);
      if (i < 7) {
        Serial.print(", ");
      }
    }
    if ( OneWire::crc8( addr, 7) != addr[7]) {
        Serial.print("CRC is not valid!\n");
        return;
    }
  }
  Serial.print("\n\r\n\rThat's it.\r\n");
  ds.reset_search();
  return;
}

void getGeyserTemperatureInstance()  {
  // This function is used to capture all the temperature measurements from the system
  // Most of the temperature sensors will be on the oneWire bus, but additional analog
  // Sensors can also be added or used 

  // Extract temperature measurements from the onewire sensors
  if (firstRequest) 
  {
    requestAllTemperatureConversions();
    firstRequest = false;
  }
  else    
  {
    getGeyserTemperatures();  // Fetches the temperature data from sensor scratch pads and stores it in a matrix of doubles
    requestAllTemperatureConversions(); // Sends a Skip ROM command to communicate with all sensor simultaneously to do a ADC conversion
  }
}

void requestAllTemperatureConversions() {
  // This function is used to send a skip ROM and temperature read command to all DS18B20 busses
  for (int i = 0; i < boilerProfileBusNumber; i++)
    {
      TempBusses[i].requestTemperatures();
    }
}

void captureGeyserTemperatures()  {

  for(int i = 0; i < boilerProfileBusNumber; i++)
  {
    for(int j = 0; j < maxSensorsPerBus; j++)
    {
      boilerProfileTempAray[i][j] = TempBusses[i].getTempC(boilerProfileTempReg[i][j]);
    }
  }
}