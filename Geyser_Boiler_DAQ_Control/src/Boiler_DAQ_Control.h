#ifndef Boiler_DAQ_Control_H
#define Boiler_DAQ_Control_H

//include all necessary libraries
#include <Arduino.h>
#include "Boiler_DAQ_Control.h"
#include <Wire.h>
#include <OneWire.h>
#include <SPI.h>
#include <SD.h>
#include <DallasTemperature.h>
#include "RTClib.h"
#include <rtc_clock.h>
#include "temp_sensor_address_reg.h"

/** Define all pins used by the microcontroller **/
#define oneWireBus1             30
#define oneWireBus2             31
#define oneWireBus3             32
#define oneWireBus4             33
#define oneWireBus5             34
#define oneWireBus6             35       
#define oneWireBus7             36       
#define oneWireBus8             37       
#define sd_CS                   14

/** Define general parameters used by source code **/
int sensorErrorNum = DEVICE_DISCONNECTED_C;
const uint32_t SerialBaudrate = 115200;  // Baud rate for serial communication
uint8_t tempDeviceCount[boilerProfileBusNumber] = {};
uint8_t tempResolution = 12;

/** Define all flags **/
bool captureBoilerData = false;
bool sdCardSuccessFlag = false;
bool newFileFlag = true;
bool firstRequest = true;

/** Define all class instances **/
// OneWire instances needed for Dallas Temperature library
OneWire OneWireBusses[8] = {OneWire(oneWireBus1), OneWire(oneWireBus2), OneWire(oneWireBus3), OneWire(oneWireBus4), \
                            OneWire(oneWireBus5), OneWire(oneWireBus6), OneWire(oneWireBus7), OneWire(oneWireBus8)};
// Temperature sensor dallasTemperature instances (Instantiated in the setup script)
DallasTemperature TempBusses[boilerProfileBusNumber] = {};

RTC_DS3231 sysRTC;  // External DS3231 RTC instance
OneWire  ds(7);  // Connect your 1-wire device to pin 3

/** Define all function prototypes **/
void oneWireSetup();
bool sdCardSetup();
void discoverOneWireDevices();
void requestAllTemperatureConversions();
void getGeyserTemperatures();
void captureGeyserTemperatureInstance();

#endif